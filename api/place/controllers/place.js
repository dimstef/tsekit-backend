'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const { sanitizeEntity } = require('strapi-utils');

async function getLast(place){
  const result = await strapi.query('population-assessment').model.query(qb => {
    qb.where({ "place": place.id })
    .orderBy('created_at', 'DESC') // Last created first
  })
  .fetch()
  //strapi.log.info("place",result)
  try{
    const resultJson = await result.toJSON()
    //strapi.log.info("resultJson",resultJson)
    return resultJson
  }catch(e){
    return null
  }
  
}

module.exports = {
  /**
   * Create a record.
   *
   * @return {Object}
   */

  async create(ctx) {
    let entity;
    ctx.request.body.owner = ctx.state.user.id;
    entity = await strapi.services.place.create(ctx.request.body);
    
    return sanitizeEntity(entity, { model: strapi.models.place });
  },
  async update(ctx) {
    const { id } = ctx.params;

    let entity;

    const [place] = await strapi.services.place.find({
      id: ctx.params.id,
      'user.id': ctx.state.user.id,
    });

    if (!place) {
      return ctx.unauthorized(`You can't update this entry`);
    }

    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.place.update({ id }, data, {
        files,
      });
    } else {
      entity = await strapi.services.place.update({ id }, ctx.request.body);
    }

    strapi.log.info(entity)
    return sanitizeEntity(entity, { model: strapi.models.place });
  },
  async history(ctx) {
    let entity;
    let entities = await strapi.query('population-assessment').find({ place: ctx.params.id });
    return entities.map(e=> {
      delete e.place
      return sanitizeEntity(e, {
        model: strapi.models['population-assessment'],
      })
    })
  },
  async find(ctx) {
    let entities;

    if (ctx.query._q) {
      entities = await strapi.services.place.search(ctx.query);
    } else {
      entities = await strapi.services.place.find(ctx.query)
    }
    
    const unresolvedPromises = Promise.all(entities.map(async entity => {
      const place = sanitizeEntity(entity, {
        model: strapi.models.place,
      });
      delete place.population_assessments;
      place.last_assessment = await getLast(place)
      return place;
    }));
    return unresolvedPromises;
  },
};
