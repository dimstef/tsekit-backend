'use strict';

/**
 * `isOwnerOfPlace` policy.
 */

module.exports = async (ctx, next) => {
  const {id, role} = ctx.state.user;
  //strapi.log.info(id,role)
  await next();
  strapi.log.info(JSON.stringify(ctx.response.body))
  if(ctx.params.id){
     let owner = ctx.response.body.user.id
     if(owner !== id && role !== "administrator"){
        return ctx.unauthorized("You are not allowed to perform this action.");
     }
  }
};